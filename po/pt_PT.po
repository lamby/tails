# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# MS <manuelarodsilva@gmail.com>, 2018
# Rui <xymarior@yandex.com>, 2019
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-14 21:20+0100\n"
"PO-Revision-Date: 2019-01-17 11:15+0000\n"
"Last-Translator: Rui <xymarior@yandex.com>\n"
"Language-Team: Portuguese (Portugal) (http://www.transifex.com/otf/"
"torproject/language/pt_PT/)\n"
"Language: pt_PT\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:39
msgid "Tor is ready"
msgstr "Tor está pronto"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:40
msgid "You can now access the Internet."
msgstr "Agora pode aceder à Internet."

#: config/chroot_local-includes/etc/whisperback/config.py:69
#, python-format
msgid ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>\n"
"Giving us an email address allows us to contact you to clarify the problem. "
"This\n"
"is needed for the vast majority of the reports we receive as most reports\n"
"without any contact information are useless. On the other hand it also "
"provides\n"
"an opportunity for eavesdroppers, like your email or Internet provider, to\n"
"confirm that you are using Tails.\n"
"</p>\n"
msgstr ""
"<h1>Ajude-nos a corrigir o erro!</h1>\n"
"<p>Leia <a href=\"%s\">as nossas instruções para reportar erros</a>.</p>\n"
"<p><strong>Não inclua mais informação pessoal do que a\n"
"necessária!</strong></p>\n"
"<h2>Sobre dar-nos um endereço de correio eletrónico</h2>\n"
"<p>\n"
"Ao dar-nos um endereço de correio eletrónico permite-nos contactá-lo para "
"clarificar o problema. Isto é necessário para a grande maioria dos "
"relatórios que nós recebemos, uma vez que sem informação de contacto, eles "
"são inúteis. Por outro lado, este também proporciona uma oportunidade para "
"escutas, tal como o seu provedor de correio eletrónico ou de Internet, para "
"confirmarem que está a utilizar o Tails.\n"
"</p>\n"

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:51
msgid ""
"You can install additional software automatically from your persistent "
"storage when starting Tails."
msgstr ""
"Pode adicionar programas automaticamente do seu armazenamento permanente "
"quando iniciar o Tails."

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:77
msgid ""
"The following software is installed automatically from your persistent "
"storage when starting Tails."
msgstr ""
"O seguinte programa é instalado automaticamente do seu armazenamento "
"permanente quando o Tails é iniciado."

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:132
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:172
msgid ""
"To add more, install some software using <a href=\"synaptic.desktop"
"\">Synaptic Package Manager</a> or <a href=\"org.gnome.Terminal.desktop"
"\">APT on the command line</a>."
msgstr ""
"Para adicionar outros, instale programas utilizando o <a href=\"synaptic."
"desktop\">Gestor de Pacotes Synaptic</a> ou o <a href=\"org.gnome.Terminal."
"desktop\">APT na linha de comandos</a>."

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:151
msgid "_Create persistent storage"
msgstr "_Criar armazenamento permanente"

#: config/chroot_local-includes/usr/local/bin/electrum:57
msgid "Persistence is disabled for Electrum"
msgstr "A persistência está desativada para o Electrum"

#: config/chroot_local-includes/usr/local/bin/electrum:59
msgid ""
"When you reboot Tails, all of Electrum's data will be lost, including your "
"Bitcoin wallet. It is strongly recommended to only run Electrum when its "
"persistence feature is activated."
msgstr ""
"Quando reinicia o Tails, todos os dados do Electrum serão perdidos, "
"incluindo a sua carteira Bitcoin. Recomenda-se que execute apenas o Electrum "
"se a funcionalidade de persistência estiver ativada."

#: config/chroot_local-includes/usr/local/bin/electrum:60
msgid "Do you want to start Electrum anyway?"
msgstr "Mesmo assim, deseja iniciar o Electrum?"

#: config/chroot_local-includes/usr/local/bin/electrum:63
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:41
msgid "_Launch"
msgstr "_Iniciar"

#: config/chroot_local-includes/usr/local/bin/electrum:64
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:42
msgid "_Exit"
msgstr "_Sair"

#: config/chroot_local-includes/usr/local/bin/keepassx:17
#, sh-format
msgid ""
"<b><big>Do you want to rename your <i>KeePassX</i> database?</big></b>\n"
"\n"
"You have a <i>KeePassX</i> database in your <i>Persistent</i> folder:\n"
"\n"
"<i>${filename}</i>\n"
"\n"
"Renaming it to <i>keepassx.kdbx</i> would allow <i>KeePassX</i> to open it "
"automatically in the future."
msgstr ""
"<b><big>Quer alterar o nome das sua base de dados do <i>KeePassX</i>?</big></"
"b>\n"
"\n"
"Tem uma base de dados do <i>KeePassX</i> na sua pasta <i>Permanente</i>:\n"
"\n"
"<i>${filename}</i>\n"
"\n"
"Se alterar o nome para <i>keepassx.kdbx</i> irá permitir que o <i>KeePassX</"
"i> a abra automaticamente daqui para a frente."

#: config/chroot_local-includes/usr/local/bin/keepassx:25
msgid "Rename"
msgstr "Renomear"

#: config/chroot_local-includes/usr/local/bin/keepassx:26
msgid "Keep current name"
msgstr "Manter nome atual"

#: config/chroot_local-includes/usr/local/bin/replace-su-with-sudo:21
msgid "su is disabled. Please use sudo instead."
msgstr "O su está desativado. Em vez disso utilize o sudo."

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:75
msgid "Restart"
msgstr "Reiniciar"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:78
msgid "Lock screen"
msgstr "Ecrã de bloqueio"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:81
msgid "Power Off"
msgstr "Desligar"

#: config/chroot_local-includes/usr/local/bin/tails-about:22
#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:1
msgid "Tails"
msgstr "Tails"

#: config/chroot_local-includes/usr/local/bin/tails-about:25
#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:1
msgid "About Tails"
msgstr "Sobre o Tails"

#: config/chroot_local-includes/usr/local/bin/tails-about:35
msgid "The Amnesic Incognito Live System"
msgstr "O Sistema Live Incógnito Amnésico"

#: config/chroot_local-includes/usr/local/bin/tails-about:36
#, python-format
msgid ""
"Build information:\n"
"%s"
msgstr ""
"Informação da compilação:\n"
"%s"

#: config/chroot_local-includes/usr/local/bin/tails-about:54
msgid "not available"
msgstr "não disponível"

#. Translators: Don't translate {details}, it's a placeholder and will
#. be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:147
#, python-brace-format
msgid ""
"{details} Please check your list of additional software or read the system "
"log to understand the problem."
msgstr ""
"{details} Por favor verifique a sua lista de programas adicionais ou leia o "
"registo do sistema para entender o problema."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:152
msgid ""
"Please check your list of additional software or read the system log to "
"understand the problem."
msgstr ""
"Por favor verifique sua lista de programas adicionais ou leia o registo do "
"sistema para entender o problema."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:156
msgid "Show Log"
msgstr "Mostrar Registo"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:156
msgid "Configure"
msgstr "Configurar"

#. Translators: Don't translate {beginning} or {last}, they are
#. placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:222
#, python-brace-format
msgid "{beginning} and {last}"
msgstr "{beginning} e {last}"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:223
msgid ", "
msgstr ", "

#. Translators: Don't translate {packages}, it's a placeholder and will
#. be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:289
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:319
#, python-brace-format
msgid "Add {packages} to your additional software?"
msgstr "Adicionar {packages} aos seus programas adicionais?"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:291
msgid ""
"To install it automatically from your persistent storage when starting Tails."
msgstr ""
"Para instalá-lo automaticamente a partir de seu armazenamento permanente ao "
"iniciar o Tails."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:293
msgid "Install Every Time"
msgstr "Instalar Sempre"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:294
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:325
msgid "Install Only Once"
msgstr "Instalar Uma Vez"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:300
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:330
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:371
msgid "The configuration of your additional software failed."
msgstr "A configuração do seu software adicional falhou."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:321
msgid ""
"To install it automatically when starting Tails, you can create a persistent "
"storage and activate the <b>Additional Software</b> feature."
msgstr ""
"Para instalá-lo automaticamente ao iniciar o Tails, pode criar um "
"armazenamento permanente e ativar o recurso <b>Programa Adicional</b>."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:324
msgid "Create Persistent Storage"
msgstr "Criar armazenamento permanente"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:332
msgid "Creating your persistent storage failed."
msgstr "A criação do seu armazenamento permanente falhou."

#. Translators: Don't translate {packages}, it's a placeholder and
#. will be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:341
#, python-brace-format
msgid "You could install {packages} automatically when starting Tails"
msgstr "Pode instalar {packages} automaticamente ao iniciar o Tails."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:344
msgid ""
"To do so, you need to run Tails from a USB stick installed using <i>Tails "
"Installer</i>."
msgstr ""
"Para fazê-lo, tem de executar o Tails a partir de um drive USB instalado "
"utilizando o <i>Instalador do Tails</i>."

#. Translators: Don't translate {packages}, it's a placeholder and will be
#. replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:359
#, python-brace-format
msgid "Remove {packages} from your additional software?"
msgstr "Remover {packages} dos seus programas adicionais?"

#. Translators: Don't translate {packages}, it's a placeholder
#. and will be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:363
#, python-brace-format
msgid "This will stop installing {packages} automatically."
msgstr "Isto interromperá a instalação automática de {packages}."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:365
msgid "Remove"
msgstr "Remover"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:366
#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:119
#: config/chroot_local-includes/usr/local/bin/tor-browser:46
msgid "Cancel"
msgstr "Cancelar"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:544
msgid "Installing your additional software from persistent storage..."
msgstr ""
"A instalar o seu programa adicional a partir do armazenamento permanente..."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:546
msgid "This can take several minutes."
msgstr "Isto pode demorar vários minutos."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:559
msgid "The installation of your additional software failed"
msgstr "A instalação do seu programa adicional falhou"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:574
msgid "Additional software installed successfully"
msgstr "Programa adicional instalado com sucesso"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:594
msgid "The check for upgrades of your additional software failed"
msgstr "A verificação de atualizações do seu programa adicional falhou"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:596
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:604
msgid ""
"Please check your network connection, restart Tails, or read the system log "
"to understand the problem."
msgstr ""
"Por favor verifique a sua ligação de rede, reinicie o Tails ou leia os "
"registos do sistema para entender o problema."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:603
msgid "The upgrade of your additional software failed"
msgstr "Falha na atualização do seu software adicional."

#: config/chroot_local-includes/usr/local/lib/tails-additional-software-notify:37
msgid "Documentation"
msgstr "Documentação"

#. Translators: Don't translate {package}, it's a placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:96
#, python-brace-format
msgid ""
"Remove {package} from your additional software? This will stop installing "
"the package automatically."
msgstr ""
"Remover {package} dos seus programas adicionais? Isto interromperá "
"automaticamente a instalação do pacote."

#. Translators: Don't translate {pkg}, it's a placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:107
#, python-brace-format
msgid "Failed to remove {pkg}"
msgstr "Não foi possível remover {pkg}"

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:124
msgid "Failed to read additional software configuration"
msgstr "Falha na leitura da configuração do programa adicional."

#. Translators: Don't translate {package}, it's a placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:154
#, python-brace-format
msgid "Stop installing {package} automatically"
msgstr "Parar de instalar {package} automaticamente"

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:179
msgid ""
"To do so, install some software using <a href=\"synaptic.desktop\">Synaptic "
"Package Manager</a> or <a href=\"org.gnome.Terminal.desktop\">APT on the "
"command line</a>."
msgstr ""
"Para fazê-lo, instale programas utilizando o <a href=\"synaptic.desktop"
"\">Gestor de Pacotes Synaptic</a> ou o <a href=\"org.gnome.Terminal.desktop"
"\">APT na linha de comandos</a>."

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:188
msgid ""
"To do so, unlock your persistent storage when starting Tails and install "
"some software using <a href=\"synaptic.desktop\">Synaptic Package Manager</"
"a> or <a href=\"org.gnome.Terminal.desktop\">APT on the command line</a>."
msgstr ""
"Para fazê-lo, desbloqueie o armazenamento permanente ao iniciar o Tails e "
"instale programas utilizando o <a href=\"synaptic.desktop\">Gestor de "
"Pacotes Synaptic</a> ou o <a href=\"org.gnome.Terminal.desktop\">APT na "
"linha de comandos</a>."

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:198
msgid ""
"To do so, create a persistent storage and install some software using <a "
"href=\"synaptic.desktop\">Synaptic Package Manager</a> or <a href=\"org."
"gnome.Terminal.desktop\">APT on the command line</a>."
msgstr ""
"Para fazê-lo, crie um armazenamento permanente e instale programas "
"utilizando o <a href=\"synaptic.desktop\">Gestor de Pacotes Synaptic</a> ou "
"o <a href=\"org.gnome.Terminal.desktop\">APT na linha de comandos</a>."

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:206
msgid ""
"To do so, install Tails on a USB stick using <a href=\"tails-installer."
"desktop\">Tails Installer</a> and create a persistent storage."
msgstr ""
"Para fazê-lo, instale o Tails num drive USB utilizando o <a href=\"tails-"
"installer.desktop\">Instalador do Tails</a> e crie um armazenamento "
"permanente."

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:253
msgid "[package not available]"
msgstr "[pacote não disponível]"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:52
msgid "Synchronizing the system's clock"
msgstr "A sincronizar o relógio do sistema"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:53
msgid ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."
msgstr ""
"O Tor precisa de um relógio preciso para funcionar corretamente, "
"especialmente para os 'Serviços Ocultos'. Por favor, aguarde..."

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:87
msgid "Failed to synchronize the clock!"
msgstr "A sincronização do relógio falhou!"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:124
msgid "This version of Tails has known security issues:"
msgstr "Esta versão do Tails tem alguns problemas de segurança conhecidos:"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:134
msgid "Known security issues"
msgstr "Problemas de segurança conhecidos"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:52
#, sh-format
msgid "Network card ${nic} disabled"
msgstr "Placa de rede ${nic} desativada"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:53
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}) so it is "
"temporarily disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"Mascarar endereço MAC para o cartão da rede ${nic_name} (${nic}) falhou, por "
"isso está temporariamente desativada.\n"
"Pode preferir reiniciar o Tails e desativar a máscara do endereço MAC."

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:62
msgid "All networking disabled"
msgstr "Toda a rede desativada"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:63
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}). The error "
"recovery also failed so all networking is disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"Mascarar endereço MAC para o cartão da rede ${nic_name} (${nic}) falhou. A "
"recuperação de erro também falhou, por isso toda a rede está desativada.\n"
"Pode preferir reiniciar o Tails e desativar a máscara do endereço MAC."

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:110
msgid "Lock Screen"
msgstr "Ecrã de Bloqueio"

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:125
msgid "Screen Locker"
msgstr "Bloqueador de Ecrã"

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:131
msgid "Set up a password to unlock the screen."
msgstr "Configure uma palavra-passe para desbloquear o ecrã."

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:136
msgid "Password"
msgstr "Palavra-passe"

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:142
msgid "Confirm"
msgstr "Confirmar"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:35
msgid ""
"\"<b>Not enough memory available to check for upgrades.</b>\n"
"\n"
"Make sure this system satisfies the requirements for running Tails.\n"
"See file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Try to restart Tails to check for upgrades again.\n"
"\n"
"Or do a manual upgrade.\n"
"See https://tails.boum.org/doc/first_steps/upgrade#manual\""
msgstr ""
"\"<b>Não há memória suficiente para procurar por atualizações.</b>\n"
"\n"
"Verifique se o sistema cumpre os requerimentos para executar o Tails\n"
"Consulte o file:///usr/share/doc/tails/website/doc/about/requirements.en."
"html\n"
"\n"
"Tente reiniciar o Tails para procurar por atualizações novamente\n"
"\n"
"Ou faça a atualização manualmente\n"
"Consulte https://tails.boum.org/doc/first_steps/upgrade#manual\""

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:72
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:27
msgid "error:"
msgstr "erro:"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:73
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:28
msgid "Error"
msgstr "Erro"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:71
msgid "Warning: virtual machine detected!"
msgstr "Atenção: detetada máquina virtual!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:74
msgid "Warning: non-free virtual machine detected!"
msgstr "Aviso: detetada máquina virtual não livre!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:77
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails. Only free software can be considered "
"trustworthy, for both the host operating system and the virtualization "
"software."
msgstr ""
"O sistema operativo hospedeiro e o software de virtualização podem "
"monitorizar o que está a fazer no Tails. Apenas o software livre pode ser "
"considerado como de confiança, para ambos."

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:81
msgid "Learn more"
msgstr "Saber mais"

#: config/chroot_local-includes/usr/local/bin/tor-browser:43
msgid "Tor is not ready"
msgstr "O Tor não está pronto"

#: config/chroot_local-includes/usr/local/bin/tor-browser:44
msgid "Tor is not ready. Start Tor Browser anyway?"
msgstr "O Tor não está pronto. Mesmo assim, iniciar o Tor Browser?"

#: config/chroot_local-includes/usr/local/bin/tor-browser:45
msgid "Start Tor Browser"
msgstr "Iniciar o Tor Browser"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/torstatus@tails.boum.org/extension.js:40
msgid "Tor"
msgstr "Tor"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/torstatus@tails.boum.org/extension.js:55
msgid "Open Onion Circuits"
msgstr "Abrir Circuitos Onion"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:38
msgid "Do you really want to launch the Unsafe Browser?"
msgstr "Quer mesmo iniciar o Navegador Inseguro?"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:40
msgid ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>.\\nOnly "
"use the Unsafe Browser if necessary, for example\\nif you have to login or "
"register to activate your Internet connection."
msgstr ""
"A atividade de rede no Navegador Inseguro <b>não é anónima</b>.\\nUtilize "
"apenas o Navegador Inseguro se necessário, por exemplo,\\nse tiver que "
"iniciar a sessão ou registar-se para ativar a sua ligação à Internet."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:51
msgid "Starting the Unsafe Browser..."
msgstr "A iniciar o Navegador Inseguro..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:52
msgid "This may take a while, so please be patient."
msgstr "Isto pode demorar algum tempo, por favor, seja paciente."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:57
msgid "Shutting down the Unsafe Browser..."
msgstr "A encerrar o Navegador Inseguro..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:58
msgid ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."
msgstr ""
"Isto pode demorar algum tempo e não deve reiniciar o Navegador Inseguro até "
"que este tenha sido propriamente encerrado."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:70
msgid "Failed to restart Tor."
msgstr "Não foi possível reiniciar o Tor:"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:84
#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:1
msgid "Unsafe Browser"
msgstr "Navegador Inseguro"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:91
msgid ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."
msgstr ""
"Já está em execução outro Navegador Inseguro, ou a ser encerrado. Por favor, "
"tente novamente daqui a pouco."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:99
msgid "Failed to setup chroot."
msgstr "Não foi possível configurar o chroot."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:104
msgid "Failed to configure browser."
msgstr "Não foi possível configurar o navegador."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:110
msgid ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."
msgstr ""
"Não foi obtido nenhum servidor DNS através do DHCP ou configurado "
"manualmente com o Gestor de Redes."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:121
msgid "Failed to run browser."
msgstr "Não foi possível executar o navegador."

#. Translators: Don't translate {volume_label} or {volume_size},
#. they are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:58
#, python-brace-format
msgid "{volume_label} ({volume_size})"
msgstr "{volume_label} ({volume_size})"

#. Translators: Don't translate {partition_name} or {partition_size},
#. they are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:63
#, python-brace-format
msgid "{partition_name} ({partition_size})"
msgstr "{partition_name} ({partition_size})"

#. Translators: Don't translate {volume_size}, it's a placeholder
#. and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:68
#, python-brace-format
msgid "{volume_size} Volume"
msgstr "{volume_size} Volume"

#. Translators: Don't translate {volume_name}, it's a placeholder and
#. will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:107
#, python-brace-format
msgid "{volume_name} (Read-Only)"
msgstr "{volume_name} (apenas leitura)"

#. Translators: Don't translate {partition_name} and {container_path}, they
#. are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:115
#, python-brace-format
msgid "{partition_name} in {container_path}"
msgstr "{partition_name} em {container_path}"

#. Translators: Don't translate {volume_name} and {path_to_file_container},
#. they are placeholders and will be replaced. You should only have to translate
#. this string if it makes sense to reverse the order of the placeholders.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:122
#, python-brace-format
msgid "{volume_name} – {path_to_file_container}"
msgstr "{volume_name} – {path_to_file_container}"

#. Translators: Don't translate {partition_name} and {drive_name}, they
#. are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:128
#, python-brace-format
msgid "{partition_name} on {drive_name}"
msgstr "{partition_name} em {drive_name}"

#. Translators: Don't translate {volume_name} and {drive_name},
#. they are placeholders and will be replaced. You should only have to translate
#. this string if it makes sense to reverse the order of the placeholders.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:135
#, python-brace-format
msgid "{volume_name} – {drive_name}"
msgstr "{volume_name} – {drive_name}"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:222
msgid "Wrong passphrase or parameters"
msgstr "Frase-senha ou parâmetros incorretos"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:224
msgid "Error unlocking volume"
msgstr "Erro ao desbloquear o volume"

#. Translators: Don't translate {volume_name} or {error_message},
#. they are placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:228
#, python-brace-format
msgid ""
"Couldn't unlock volume {volume_name}:\n"
"{error_message}"
msgstr ""
"Não foi possível desbloquear o volume {volume_name}:\n"
"{error_message}"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_list.py:83
msgid "No file containers added"
msgstr "Não foram adicionados nenhuns volumes de ficheiros"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_list.py:98
msgid "No VeraCrypt devices detected"
msgstr "Não foram detetados dispositivos VeraCrpyt"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:114
msgid "Container already added"
msgstr "Volume já adicionado"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:115
#, python-format
msgid "The file container %s should already be listed."
msgstr "O volume de ficheiros %s já está na lista."

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:131
msgid "Container opened read-only"
msgstr "Volume aberto no modo de apenas leitura"

#. Translators: Don't translate {path}, it's a placeholder  and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:133
#, python-brace-format
msgid ""
"The file container {path} could not be opened with write access. It was "
"opened read-only instead. You will not be able to modify the content of the "
"container.\n"
"{error_message}"
msgstr ""
"Não foi possível abrir o volume de ficheiros {path} com permissão de "
"escrita. Em vez disso, foi aberto apenas no modo de leitura. Não poderá "
"alterar o conteúdo do volume.\n"
"{error_message}"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:138
msgid "Error opening file"
msgstr "Erro ao abrir o ficheiro"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:160
msgid "Not a VeraCrypt container"
msgstr "Não é um volume VeraCrypt"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:161
#, python-format
msgid "The file %s does not seem to be a VeraCrypt container."
msgstr "O ficheiro %s não parece ser um volume VeraCrypt."

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:163
msgid "Failed to add container"
msgstr "O adicionar do volume falhou"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:164
#, python-format
msgid ""
"Could not add file container %s: Timeout while waiting for loop setup.Please "
"try using the <i>Disks</i> application instead."
msgstr ""
"Não foi possível adicionar o volume de ficheiros %s: Tempo de espera "
"esgotado ao aguardar o loop de configuração. Por favor, experimente usar em "
"vez disso o programa <i>Discos</i>."

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:209
msgid "Choose File Container"
msgstr "Escolher Volume de Ficheiros"

#: ../config/chroot_local-includes/etc/skel/Desktop/Report_an_error.desktop.in.h:1
msgid "Report an error"
msgstr "Comunicar um erro"

#: ../config/chroot_local-includes/etc/skel/Desktop/tails-documentation.desktop.in.h:1
#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:1
msgid "Tails documentation"
msgstr "Documentação do Tails"

#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:2
msgid "Learn how to use Tails"
msgstr "Aprenda a utilizar o Tails"

#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:2
msgid "Learn more about Tails"
msgstr "Saber mais sobre Tails"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:1
msgid "Tor Browser"
msgstr "Tor Browser"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:2
msgid "Anonymous Web Browser"
msgstr "Navegador da Web Anónimo"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:2
msgid "Browse the World Wide Web without anonymity"
msgstr "Explore a World Wide Web sem anonimato"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:3
msgid "Unsafe Web Browser"
msgstr "Navegador Web Inseguro"

#: ../config/chroot_local-includes/usr/share/applications/unlock-veracrypt-volumes.desktop.in.h:1
msgid "Unlock VeraCrypt Volumes"
msgstr "Desbloquear Volumes VeraCrypt"

#: ../config/chroot_local-includes/usr/share/applications/unlock-veracrypt-volumes.desktop.in.h:2
msgid "Mount VeraCrypt encrypted file containers and devices"
msgstr "Montar volumes de ficheiros e dispositivos VeraCrypt"

#: ../config/chroot_local-includes/usr/share/applications/org.boum.tails.additional-software-config.desktop.in.h:1
msgid "Additional Software"
msgstr "Programas Adicionais"

#: ../config/chroot_local-includes/usr/share/applications/org.boum.tails.additional-software-config.desktop.in.h:2
msgid ""
"Configure the additional software installed from your persistent storage "
"when starting Tails"
msgstr ""
"Configure o software adicional instalado a partir do seu armazenamento "
"permanente ao iniciar o Tails."

#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:2
msgid "Tails specific tools"
msgstr "Ferramentas específicas do Tails"

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.root-terminal.policy.in.h:1
msgid "To start a Root Terminal, you need to authenticate."
msgstr "Para iniciar um Terminal Root precisa de autenticar-se."

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.additional-software.policy.in.h:1
msgid "Remove an additional software package"
msgstr "Remover um pacote de software adicional"

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.additional-software.policy.in.h:2
msgid ""
"Authentication is required to remove a package from your additional software "
"($(command_line))"
msgstr ""
"É necessária a autenticação para remover um pacote dos seus programas "
"adicionais ($(command_line))"

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/main.ui.in:61
msgid "File Containers"
msgstr "Volumes de Ficheiros"

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/main.ui.in:80
msgid "_Add"
msgstr "_Adicionar"

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/main.ui.in:86
msgid "Add a file container"
msgstr "Adicionar um volume de ficheiros"

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/main.ui.in:103
msgid "Partitions and Drives"
msgstr "Partições e Unidades"

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/main.ui.in:121
msgid ""
"This application is not affiliated with or endorsed by the VeraCrypt project "
"or IDRIX."
msgstr ""
"Este programa não é afiliado nem endossado pelo Projeto VeraCrypt ou pela "
"IDRIX."

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/volume.ui.in:38
msgid "Lock this volume"
msgstr "Bloquear este volume"

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/volume.ui.in:61
msgid "Detach this volume"
msgstr "Desanexar este volume"

#: ../config/chroot_local-includes/usr/local/share/mime/packages/unlock-veracrypt-volumes.xml.in.h:1
msgid "TrueCrypt/VeraCrypt container"
msgstr "Volume TrueCrypt/VeraCrypt"
