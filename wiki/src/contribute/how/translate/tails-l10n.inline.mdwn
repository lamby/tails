Translators coordinate on the tails-l10n mailing
list (*l10n* stands for [[!wikipedia Software_localization desc="localization"]]).

This is where important changes to translatable strings are announced.
So, please subscribe to the list if you want to become a regular
translator:

Public archive of tails-l10n: <https://lists.autistici.org/list/tails-l10n.html>.

<form method="POST" action="https://www.autistici.org/mailman/subscribe/tails-l10n">
	<input class="text" name="email" value=""/>
	<input class="button" type="submit" value="Subscribe"/>
</form>
